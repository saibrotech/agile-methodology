# Agile Methodology

Processes and definition of Agile Methodology in SaibroTech.

# Kanban

## Implantação gradual

Focar em ir implantando o Kanban aos poucos num processo contínuo de aprendizado e considerando a realização de um piloto e o passo-a-passo descrito abaixo:

1. Visualizar o trabalho
1. Mapear o fluxo
1. Selecionar as informações dos cartões de acordo com as métricas
1. Começar com um quadro físico
1. Começar com um processo mais simples dentro da GOFIRST
1. Mapear Lead Time e Throughput por X semanas
1. Reuniões diárias
1. Melhorar continuamente
1. Criar políticas para tratar itens urgentes
1. Ter raia para itens urgentes
1. Limitar WIP


## Erros mais comuns

- Andar contra o sentido do fluxo.
- Ter uma coluna para “on hold” ou espera.
- Não ter limite ou não respeitar o WIP.
- Não mapear todos os itens de trabalho ou o post-it “fantasma”
- Não usar métricas de forma consistente ou relevante.
- Não deixar a métrica ser imposta, o time precisa estar envolvido na decisão!
- Punir o time caso o resultado piore, premiar caso o resultado melhore
